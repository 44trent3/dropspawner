package com.minecraftonline.dropspawner;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.spongepowered.api.Game;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.plugin.Plugin;

@Plugin(
        id = "dropspawner",
        name = "DropSpawner",
        description = "Plugin to allow spawners to broken and picked up.",
        authors = {
                "44trent3"
        }
)
public class DropSpawner {

    @Inject
    private Logger logger;

    @Listener
    public void init(GameInitializationEvent event) {
        Sponge.getEventManager().registerListeners(this, new SpawnerBreakListener());
    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        logger.info("DropSpawner 1.0 has been loaded.");
    }
}
