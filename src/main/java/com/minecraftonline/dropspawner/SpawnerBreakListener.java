package com.minecraftonline.dropspawner;

import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.entity.SpawnEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.Extent;
import java.util.Optional;

public class SpawnerBreakListener {
    @Listener
    public void breakListener(ChangeBlockEvent.Break event, @First Player player) {
        for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
            if (transaction.getOriginal().getState().getType() == BlockTypes.MOB_SPAWNER) {
                Optional<Location<World>> optLocation = transaction.getOriginal().getLocation();
                if (optLocation.isPresent()) {
                    Vector3i position = optLocation.get().getBlockPosition();
                    Extent extent = optLocation.get().getExtent();
                    Entity item = extent.createEntity(EntityTypes.ITEM, position);
                    item.offer(Keys.REPRESENTED_ITEM, stack().createSnapshot());
                    if (player.gameMode().get().equals(GameModes.SURVIVAL)) {
                        extent.spawnEntity(item);
                        player.sendMessage(Text.of(TextColors.GOLD, "Mined spawner. If you place it again, use /setspawner to set the mob."));
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public ItemStack stack() {
        ItemStack spawnerStack = ItemStack.builder().itemType(ItemTypes.MOB_SPAWNER).build();
        return spawnerStack;
    }

    @Listener
    public void noSpawnerXP(SpawnEntityEvent event, @Root BlockSnapshot snapshot) {
        if (snapshot.getLocation().isPresent() && snapshot.getState().getType() == BlockTypes.MOB_SPAWNER) {
            event.setCancelled(true); // Prevent the spawner from dropping XP to prevent XP farming it.
        }
    }
}